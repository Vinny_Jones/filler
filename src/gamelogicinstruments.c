/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gamelogicinstruments.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 16:23:55 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/24 16:23:57 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		onside(t_pair a, t_pair b, t_pair c)
{
	return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x) > 0);
}

int		ismap(int x, int y, t_meta *meta)
{
	return (!(x < 0 || x >= meta->bheight || y < 0 || y >= meta->bwidth));
}
