/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:39 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:41:40 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tools.h"

int		isframe(t_meta *meta, int x, int y)
{
	if (!ismap(x, y, meta))
		return (0);
	return (x == 0 || y == 0 || \
			x == meta->bheight - 1 || y == meta->bwidth - 1);
}

int		validmapcell(t_meta *meta, int x, int y)
{
	if (!ismap(x, y, meta))
		return (0);
	if (meta->map[x][y] != '.' && meta->map[x][y] != meta->charlower && \
		meta->map[x][y] != meta->charupper && meta->map[x][y] != '#')
		return (0);
	return (1);
}

int		getonside(t_meta *meta, t_coords p)
{
	t_pair	center;
	t_pair	fstart;

	center = coordstopair(&meta->logic->center);
	fstart = coordstopair(&meta->logic->fstart);
	return (onside(center, fstart, coordstopair(&p)));
}

double	getdist(t_coords *a, t_coords *b)
{
	double	a1;
	double	b1;

	a1 = a->x - b->x;
	b1 = a->y - b->y;
	return (ft_sqrt((int)ft_round(a1 * a1) + (int)ft_round(b1 * b1), 2));
}

double	getdistpair(t_pair *a, t_pair *b)
{
	t_coords	a1;
	t_coords	b1;

	a1 = pairtocoords(a);
	b1 = pairtocoords(b);
	return (getdist(&a1, &b1));
}
