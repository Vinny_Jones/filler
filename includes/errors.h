/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:40:21 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:40:22 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERRORS_H
# define ERRORS_H

# define ERR1 "Can't read from input."
# define ERR2 "Empty input."
# define ERR3 "Invalid player number."
# define ERR4 "Invalid input."
# define ERR5 "Wrong map data."
# define ERR6 "Couldn't allocate memory."
# define ERR7 "Wrong figure data."

int		printerror(const char *errormsg);

#endif
