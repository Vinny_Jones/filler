/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/01 12:12:14 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/21 12:50:23 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		ft_printf(const char *format, ...)
{
	va_list		ap;
	va_list		ap_first;
	int			count;
	char		*fmt;

	count = 0;
	va_start(ap, format);
	while (*format)
	{
		if (*format != '%')
		{
			ft_putchar(*format++);
			count++;
			continue ;
		}
		va_start(ap_first, format);
		fmt = ft_strndup(format, fmt_len((char *)format));
		count += print_arg(ap_first, ap, fmt, ft_strlen(fmt));
		fmt_fin(ap_first, ap, fmt, count);
		format += ft_strlen(fmt);
		free(fmt);
		va_end(ap_first);
	}
	va_end(ap);
	return (count);
}

int		print_arg(va_list ap_first, va_list ap, char *fmt, int len)
{
	char	spec;
	int		num_arg;
	int		width;
	int		prec;

	if (!fmt)
		return (0);
	width = getwidth(ap_first, ap, fmt, len - 1);
	prec = getprecision(ap_first, ap, fmt, len - 2);
	skip_arg(ap, fmt);
	num_arg = fmt_isnumarg(fmt);
	if ((spec = ft_tolower(fmt[--len])) == 'd' || spec == 'i')
		return (print_int(SET_AP(num_arg), fmt, width, prec));
	else if (spec == 'f')
		return (print_double(SET_AP(num_arg), fmt, width, prec));
	else if (spec == 'o' || spec == 'u' || spec == 'x' || spec == 'p')
		return (print_unsigned(SET_AP(num_arg), fmt, width, prec));
	else if (spec == 'c')
		return (print_char(SET_AP(num_arg), fmt, num_arg, width));
	else if (spec == 's')
		return (print_str(SET_AP(num_arg), fmt, width, prec));
	else if (len > 0 && (!ft_strchr(SKIP, fmt[len]) || spec == '%'))
		if (spec != 'n')
			return (print_unknown(fmt, len));
	return (0);
}

int		print_unknown(char *fmt, int len)
{
	char	spec;

	spec = fmt[len];
	fmt[len] = 'c';
	return (ft_printf(fmt, spec));
}

void	skip_arg(va_list ap, char *fmt)
{
	while (*fmt)
	{
		if (*fmt == '*' && !fmt_isnumarg(fmt))
			va_arg(ap, int);
		fmt++;
	}
}
