/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   placeFunctionsTools.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:29:27 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:29:28 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	getsidepoints(t_meta *meta, t_line *line, t_coords *res)
{
	t_coords	p;

	if (line->b)
	{
		p = makecoords(0, CALCY(0, line));
		if (p.y >= 0 && p.y < meta->bwidth)
			res[getonside(meta, p)] = p;
		p = makecoords(meta->bheight - 1, CALCY(meta->bheight - 1, line));
		if (p.y >= 0 && p.y < meta->bwidth)
			res[getonside(meta, p)] = p;
	}
	if (line->a)
	{
		p = makecoords(CALCX(0, line), 0);
		if (p.x >= 0 && p.x < meta->bheight)
			res[getonside(meta, p)] = p;
		p = makecoords(CALCX(meta->bwidth - 1, line), meta->bwidth - 1);
		if (p.x >= 0 && p.x < meta->bheight)
			res[getonside(meta, p)] = p;
	}
}

int		isfigureonline(t_meta *meta, t_pair *mp, t_line *line)
{
	t_pair	c;
	t_pair	temp;

	c.x = -1;
	while (++c.x < meta->figureheight)
	{
		c.y = -1;
		while (++c.y < meta->figurewidth)
		{
			if (meta->figure[c.x][c.y] == '.')
				continue;
			temp = makepair(mp->x + c.x, mp->y + c.y);
			if (isdotonline(line, &temp))
				return (1);
		}
	}
	return (0);
}

int		isdotonline(t_line *line, t_pair *pt)
{
	return ((!line->a || pt->x == CALCX(pt->y, line)) && \
		(!line->b || pt->y == CALCY(pt->x, line)));
}

double	getminlen(double val, t_pair *mp, t_pair *fp, t_coords *sp)
{
	t_coords	b;
	double		newval;

	b = makecoords(mp->x + fp->x, mp->y + fp->y);
	newval = getdist(sp, &b);
	return (newval < val ? newval : val);
}

double	tryplaceany(t_meta *meta, t_pair *mp)
{
	return (placefigure(meta, mp, &meta->logic->estart));
}
