/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 13:22:42 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/06 15:51:44 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*temp;
	t_list	*result;

	if (!lst)
		return (NULL);
	result = f(lst);
	temp = result;
	while ((lst = lst->next))
	{
		temp->next = f(lst);
		temp = temp->next;
	}
	return (result);
}
