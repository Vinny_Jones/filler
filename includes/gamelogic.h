/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gameLogic.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:40:08 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:40:08 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAMELOGIC_H
# define GAMELOGIC_H

# include "metainfo.h"
# define CALCX(Y, L) ((int)ft_round(-((L)->b * (Y) + (L)->c) / (L)->a))
# define CALCY(X, L) ((int)ft_round(-((L)->a * (X) + (L)->c) / (L)->b))

enum	e_flow {START = 0, PERP, FRAME};

typedef struct	s_logic {
	t_coords	fstart;
	t_coords	estart;
	t_coords	center;
	t_pair		perpexpected[2];
	t_pair		perpcurrent[2];
	int			sidereached[2];
	t_pair		perppoints[2];
	t_line		lperp;
	t_line		lmain;
	int			status;
}				t_logic;

void			rungame(t_meta *meta);
void			resolveplacement(t_meta *meta, t_pair *res, t_logic *logic);
void			outputresult(t_pair *result);

#endif
