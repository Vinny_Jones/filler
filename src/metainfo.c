/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   metaInfo.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:52 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:41:53 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		isfill(t_meta *meta, int c)
{
	return (c == meta->charlower || c == meta->charupper);
}

int		isenemyfill(t_meta *meta, int c)
{
	return (c == meta->enemycharlower || c == meta->enemycharupper);
}

void	initparams(t_meta *meta, t_logic *logic)
{
	meta->charlower = (char)((meta->playernumber == 1) ? 'o' : 'x');
	meta->charupper = (char)((meta->playernumber == 1) ? 'O' : 'X');
	meta->enemycharlower = (char)((meta->playernumber == 1) ? 'x' : 'o');
	meta->enemycharupper = (char)((meta->playernumber == 1) ? 'X' : 'O');
	meta->logic = logic;
	logic->fstart = makecoords(-1, -1);
	logic->perpcurrent[0] = makepair(-1, -1);
	logic->perpcurrent[1] = logic->perpcurrent[0];
	logic->estart = logic->fstart;
	logic->center = logic->fstart;
	logic->sidereached[0] = 0;
	logic->sidereached[1] = 0;
	logic->status = START;
	logic->lperp.a = -1;
	logic->lperp.b = -1;
	logic->lperp.c = -1;
	logic->lmain = logic->lperp;
}
