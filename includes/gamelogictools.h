/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gameLogicTools.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:28:46 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:28:46 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAMELOGICTOOLS_H
# define GAMELOGICTOOLS_H

void		checkperpplacement(t_meta *meta, t_pair *result);
t_pair		isonframe(t_meta *meta, t_pair *mp);
t_coords	calculatefigurecenter(t_meta *meta, int (*isfill)(t_meta *, int));
void		updatecurrentpoints(t_meta *meta, t_pair *res);
void		updatecurrent(t_meta *meta, t_pair mp);

#endif
