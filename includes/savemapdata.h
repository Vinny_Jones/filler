/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   saveMapData.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:39:17 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:39:31 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SAVEMAPDATA_H
# define SAVEMAPDATA_H

int		checkmapheader(t_meta *meta);
void	parsemapheader(char *line, t_pair *result);
int		savemap(t_meta *meta);
int		savemapfield(t_meta *meta);
int		savemapline(int height, char *inputline, t_meta *meta);

#endif
