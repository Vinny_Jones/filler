/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:42:14 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:42:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		printerror(const char *errormsg)
{
	ft_puterr(FMT_RED, "Error! ");
	ft_puterr(FMT_ULINE, (char *)errormsg);
	ft_puterr(NULL, "\n");
	return (0);
}
