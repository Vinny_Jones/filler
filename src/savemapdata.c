/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   saveMapData.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:43 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:41:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		checkmapheader(t_meta *meta)
{
	t_pair	size;
	char	*line;

	size = makepair(-1, -1);
	line = NULL;
	if (get_next_line(STDIN, &line) <= 0)
	{
		if (line)
			free(line);
		return (0);
	}
	parsemapheader(line, &size);
	free(line);
	if (size.x != meta->bheight || size.y != meta->bwidth)
		return (0);
	return (1);
}

void	parsemapheader(char *line, t_pair *result)
{
	if (ft_strncmp(line, "Plateau ", 8))
	{
		printerror(ERR4);
		return ;
	}
	line += 8;
	result->x = ft_atoi(line);
	while (*line && ft_isdigit(*line))
		line++;
	if (*line != ' ')
	{
		printerror(ERR5);
		return ;
	}
	result->y = ft_atoi(++line);
	while (*line && ft_isdigit(*line))
		line++;
	if (ft_strcmp(line, ":"))
	{
		printerror(ERR5);
		result->x = -1;
	}
}

int		savemap(t_meta *meta)
{
	char	*inputline;
	int		i;
	int		errorflag;

	inputline = NULL;
	if (!getlineadvanced(&inputline))
		return (0);
	errorflag = ft_strncmp(inputline, "    ", 4) ? 1 : 0;
	i = 3;
	while (!errorflag && inputline[++i])
		if (MAPHEADLINE[(i - 4) % 10] != inputline[i])
			errorflag = 1;
	free(inputline);
	if (errorflag || i - 4 != meta->bwidth)
		return (printerror(ERR5));
	return (savemapfield(meta));
}

int		savemapfield(t_meta *meta)
{
	int		height;
	char	*line;

	height = 0;
	line = NULL;
	while (height < meta->bheight && getlineadvanced(&line))
	{
		if (!savemapline(height, line, meta))
			return (0);
		line = NULL;
		height++;
	}
	if (line)
		free(line);
	return ((height == meta->bheight) ? 1 : printerror(ERR5));
}

int		savemapline(int height, char *inputline, t_meta *meta)
{
	int		errorflag;
	int		i;
	char	*mapline;

	errorflag = ((int)ft_strlen(inputline) == meta->bwidth + 4) ? 0 : 1;
	i = 0;
	while (!errorflag && ft_isdigit(inputline[i]))
		i++;
	if (!errorflag && (i != 3 || inputline[i] != ' ' || ft_atoi(inputline)
														!= height))
		errorflag = 1;
	mapline = inputline + 4;
	i = -1;
	while (!errorflag && mapline[++i])
		errorflag = (validmapchar(mapline[i]) ? 0 : 1);
	if (!errorflag)
	{
		if (meta->map[height])
			free(meta->map[height]);
		meta->map[height] = ft_strdup(mapline);
	}
	free(inputline);
	if (errorflag)
		return (printerror(ERR5));
	return (1);
}
