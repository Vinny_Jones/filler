/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:59 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:42:00 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		main(void)
{
	t_meta	meta;
	t_logic	logicparameters;

	if (initgame(&meta))
	{
		initparams(&meta, &logicparameters);
		rungame(&meta);
	}
	cleanmetainfo(&meta);
	return (0);
}
