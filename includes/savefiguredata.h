/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   saveFigureData.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:39:43 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:39:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SAVEFIGUREDATA_H
# define SAVEFIGUREDATA_H

int		savefigure(t_meta *meta);
int		savefigurefield(t_meta *meta);
int		allocfiguremap(t_meta *meta);
int		savefigureline(int height, char *inputline, t_meta *meta);
int		validmapchar(char c);

#endif
