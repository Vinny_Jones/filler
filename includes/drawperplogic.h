/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawperplogic.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 15:59:17 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/24 15:59:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DRAWPERPLOGIC_H
# define DRAWPERPLOGIC_H

void	drawperp(t_meta *meta, t_logic *logic);
void	drawhalfline(t_meta *meta, t_coords a, t_coords b);
void	findintersections(t_meta *meta, t_line *line, t_coords *res);

#endif
