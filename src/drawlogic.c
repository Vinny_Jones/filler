/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawLogic.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:29:44 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:29:45 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	createroute(t_meta *meta)
{
	t_line	main;
	t_logic	*logic;

	logic = meta->logic;
	logic->estart = calculatefigurecenter(meta, isenemyfill);
	if (logic->center.x < 0)
	{
		logic->center.x = (logic->fstart.x + logic->estart.x) / 2;
		logic->center.x = (logic->fstart.x + logic->center.x) / 2;
		logic->center.y = (logic->fstart.y + logic->estart.y) / 2;
		logic->center.y = (logic->fstart.y + logic->center.y) / 2;
	}
	main = getlinebypoints(&logic->center, &logic->estart);
	logic->lmain = main;
	logic->lperp.a = main.b;
	logic->lperp.b = -main.a;
	logic->lperp.c = main.a * logic->center.y - main.b * logic->center.x;
}

void	drawline(t_meta *meta, t_line *line, t_coords *p1, t_coords *p2)
{
	t_pair	p;
	int		i;

	i = -1;
	while (++i < meta->bheight && line->b)
	{
		p = makepair(i, CALCY(i, line));
		if (checklinecoord(meta, &p, p1, p2))
			meta->map[p.x][p.y] = '#';
	}
	i = -1;
	while (++i < meta->bwidth && line->a)
	{
		p = makepair(CALCX(i, line), i);
		if (checklinecoord(meta, &p, p1, p2))
			meta->map[p.x][p.y] = '#';
	}
}

int		checklinecoord(t_meta *meta, t_pair *p, t_coords *p1, t_coords *p2)
{
	if (!p || !ismap(p->x, p->y, meta) || meta->map[p->x][p->y] != '.')
		return (0);
	if (!p1 || !p2)
		return (1);
	return (p->x >= MIN(p1->x, p2->x) && p->x <= MAX(p1->x, p2->x) &&
			p->y >= MIN(p1->y, p2->y) && p->y <= MAX(p1->y, p2->y));
}

t_line	getlinebypoints(t_coords *m, t_coords *n)
{
	t_line	result;

	result.a = 0;
	result.b = 0;
	result.c = 0;
	if (!m || !n)
		return (result);
	result.a = n->y - m->y;
	result.b = m->x - n->x;
	result.c = m->y * (-result.b) - m->x * result.a;
	return (result);
}
