/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   placeFunctionsTools.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:28:57 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:28:57 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLACEFUNCTIONSTOOLS_H
# define PLACEFUNCTIONSTOOLS_H

void	getsidepoints(t_meta *meta, t_line *line, t_coords *res);
int		isfigureonline(t_meta *meta, t_pair *mp, t_line *line);
double	getminlen(double val, t_pair *mp, t_pair *fp, t_coords *sp);
double	tryplaceany(t_meta *meta, t_pair *mp);
int		isdotonline(t_line *line, t_pair *pt);

#endif
