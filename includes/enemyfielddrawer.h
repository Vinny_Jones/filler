/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enemyFieldDrawer.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 15:45:34 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/24 15:45:36 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENEMYFIELDDRAWER_H
# define ENEMYFIELDDRAWER_H

void	parsefield(t_meta *meta, int (*mfunc)(t_meta *, t_pair, t_pair *));
int		drawenemyframe(t_meta *meta, t_pair pt, t_pair *dir);
int		drawenemyfield(t_meta *meta, t_pair pt, t_pair *dir);
void	populatehash(t_meta *meta, int x, int y);

#endif
