/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:37:51 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:38:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOOLS_H
# define TOOLS_H

# include "filler.h"

int		isframe(t_meta *meta, int x, int y);
int		validmapcell(t_meta *meta, int x, int y);
int		getonside(t_meta *meta, t_coords p);
double	getdist(t_coords *a, t_coords *b);
double	getdistpair(t_pair *a, t_pair *b);

#endif
