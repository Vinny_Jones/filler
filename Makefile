# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 14:40:31 by apyvovar          #+#    #+#              #
#    Updated: 2017/03/12 12:08:30 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#########################     Basic     ########################################
FLAGS = -Wall -Wextra -Werror
CC = gcc $(FLAGS)
LIBDIR = libft/
LIB = $(LIBDIR)libft.a
#########################     Filler    ########################################
NAME = filler
INC = includes
SRC_DIR = src/
SRC_FILES = cleanup.c drawlogic.c errors.c gamelogic.c gamelogictools.c \
	initgame.c main.c metainfo.c placefunctions.c placefunctionstools.c \
	savefiguredata.c savemapdata.c tools.c drawperplogic.c enemyfielddrawer.c \
	gamelogicinstruments.c

SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	make -C $(LIBDIR)
	$(CC) $(OBJ) $(LIB) -o $@

.c.o: $(SRC)
	$(CC) -I $(INC) -I $(LIBDIR)$(INC) -c $^ -o $@

clean:
	make -C $(LIBDIR) clean
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(LIB)
	/bin/rm -f $(NAME)

re: fclean all
