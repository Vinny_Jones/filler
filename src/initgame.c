/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initgame.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:42:05 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:42:05 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		initgame(t_meta *meta)
{
	char	*line;
	int		error;

	line = NULL;
	error = 0;
	if (!getlineadvanced(&line) || !parseuserinfo(line, meta))
		error = 1;
	if (line)
		free(line);
	line = NULL;
	if (error || !getlineadvanced(&line) || !initiateboardsize(meta, line))
		error = 1;
	if (line)
		free(line);
	meta->figure = NULL;
	if (error || !allocboardmap(meta) || !savemap(meta) || !savefigure(meta))
		error = 1;
	return (error ? 0 : 1);
}

int		getlineadvanced(char **inputline)
{
	if (get_next_line(STDIN, inputline) < 0)
		return (printerror(ERR1));
	else if (ft_strlen(*inputline) == 0)
		return (printerror(ERR2));
	return (1);
}

int		parseuserinfo(char *inputline, t_meta *meta)
{
	if (!ft_strncmp(inputline, "$$$ exec p", 10))
		inputline += 10;
	else
		return (printerror(ERR3));
	if (ft_atoi(inputline) == 1)
		meta->playernumber = 1;
	else if (ft_atoi(inputline) == 2)
		meta->playernumber = 2;
	else
		return (printerror(ERR3));
	if (!ft_strncmp(++inputline, " : [", 4))
		inputline += 4;
	else
		return (printerror(ERR4));
	while (*inputline && *inputline != ']')
		inputline++;
	if (ft_strcmp(inputline, "]"))
		return (printerror(ERR4));
	return (1);
}

int		initiateboardsize(t_meta *meta, char *inputline)
{
	t_pair	size;

	size = makepair(-1, -1);
	parsemapheader(inputline, &size);
	if (size.x < 0 || size.y < 0)
		return (0);
	meta->bheight = size.x;
	meta->bwidth = size.y;
	return (1);
}

int		allocboardmap(t_meta *meta)
{
	int		counter;

	if (!(meta->map = MALLOC_ARRAY(char *, meta->bheight + 1)))
		return (printerror(ERR6));
	counter = 0;
	while (counter < meta->bheight)
		meta->map[counter++] = MALLOC_ARRAY(char, meta->bwidth + 1);
	meta->map[counter] = NULL;
	return (1);
}
