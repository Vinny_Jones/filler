/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   metaInfo.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:39:02 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:39:06 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef METAINFO_H
# define METAINFO_H

typedef	struct s_logic	t_logic;

typedef struct		s_metainfo {
	int				bwidth;
	int				bheight;
	int				figurewidth;
	int				figureheight;
	short int		playernumber;
	char			charlower;
	char			charupper;
	char			enemycharlower;
	char			enemycharupper;
	char			**map;
	char			**figure;
	struct s_logic	*logic;
}					t_meta;

typedef struct		s_line {
	double	a;
	double	b;
	double	c;
}					t_line;

int					isfill(t_meta *meta, int c);
int					isenemyfill(t_meta *meta, int c);
void				initparams(t_meta *meta, t_logic *logic);

#endif
