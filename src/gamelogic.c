/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gameLogic.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:42:09 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:42:10 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	rungame(t_meta *meta)
{
	t_pair	result;

	meta->logic->fstart = calculatefigurecenter(meta, isfill);
	createroute(meta);
	while (1)
	{
		result = makepair(-1, -1);
		resolveplacement(meta, &result, meta->logic);
		outputresult(&result);
		if (!checkmapheader(meta) || !savemap(meta) || !savefigure(meta))
			break ;
	}
}

void	resolveplacement(t_meta *meta, t_pair *res, t_logic *logic)
{
	if (logic->status == START)
	{
		drawline(meta, &logic->lperp, NULL, NULL);
		drawline(meta, &logic->lmain, &logic->fstart, &logic->center);
		if (tryplace(meta, res, &tryplacestart))
			return ;
	}
	if (logic->status == PERP || logic->status == START)
	{
		drawperp(meta, meta->logic);
		if (tryplace(meta, res, &tryplaceperp))
			return (checkperpplacement(meta, res));
	}
	if (logic->status == FRAME)
	{
		parsefield(meta, drawenemyframe);
		if (tryplace(meta, res, &tryplaceeside))
			return ;
		parsefield(meta, drawenemyfield);
		if (tryplace(meta, res, &tryplaceeside))
			return ;
	}
	tryplace(meta, res, &tryplaceany);
}

void	outputresult(t_pair *result)
{
	ft_printf("%i %i\n", result->x, result->y);
}
