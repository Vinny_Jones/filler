/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   placeFunctions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:29:31 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:29:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

double	placefigure(t_meta *meta, t_pair *mp, t_coords *sp)
{
	t_pair	c;
	int		i;
	double	len;

	i = 0;
	len = meta->bwidth * meta->bheight;
	c.x = -1;
	while (++c.x < meta->figureheight)
	{
		c.y = -1;
		while (++c.y < meta->figurewidth)
		{
			if (meta->figure[c.x][c.y] == '.')
				continue ;
			if (!validmapcell(meta, mp->x + c.x, mp->y + c.y))
				return (-1);
			if (isfill(meta, meta->map[mp->x + c.x][mp->y + c.y]) && ++i > 1)
				return (-1);
			if (meta->map[mp->x + c.x][mp->y + c.y] == '#' \
				|| meta->map[mp->x + c.x][mp->y + c.y] == '.')
				len = getminlen(len, mp, &c, sp);
		}
	}
	return (i != 1 ? -1 : len);
}

int		tryplace(t_meta *meta, t_pair *res, double (*foo)(t_meta *, t_pair *))
{
	t_pair	c;
	double	newmatch;
	double	match;

	match = meta->bwidth * meta->bheight;
	c.x = -meta->figureheight + 1;
	while (++c.x < meta->bheight)
	{
		c.y = -meta->figurewidth + 1;
		while (++c.y < meta->bwidth)
		{
			newmatch = foo(meta, &c);
			if (newmatch >= 0 && newmatch < match)
			{
				*res = c;
				if (newmatch == 0)
					return (1);
				match = newmatch;
			}
		}
	}
	return (match < meta->bwidth * meta->bheight);
}

double	tryplacestart(t_meta *meta, t_pair *mp)
{
	double	match;

	if ((match = placefigure(meta, mp, &meta->logic->center)) < 0)
		return (-1);
	if (isfigureonline(meta, mp, &meta->logic->lperp))
	{
		match = 0;
		meta->logic->status = PERP;
	}
	return (match);
}

double	tryplaceperp(t_meta *meta, t_pair *mp)
{
	double		match;
	double		len;
	t_coords	mpcoords;
	t_coords	newside[2];

	mpcoords = pairtocoords(mp);
	if (meta->logic->status == FRAME)
		return (-1);
	getsidepoints(meta, &meta->logic->lperp, newside);
	match = placefigure(meta, mp, &newside[getonside(meta, mpcoords)]);
	if (match < 0 || meta->logic->sidereached[getonside(meta, mpcoords)])
		return (-1);
	len = getdist(&meta->logic->center, &newside[getonside(meta, mpcoords)]);
	if (isfigureonline(meta, mp, &meta->logic->lperp))
		return (meta->bheight * meta->bwidth / 2 - (len - match));
	return (meta->bheight * meta->bwidth - (len - match));
}

double	tryplaceeside(t_meta *meta, t_pair *mp)
{
	double	match;
	int		count;
	t_pair	c;

	if ((match = placefigure(meta, mp, &meta->logic->estart) < 0))
		return (-1);
	count = 0;
	c.x = -1;
	while (++c.x < meta->figureheight)
	{
		c.y = -1;
		while (++c.y < meta->figurewidth)
			if (meta->figure[c.x][c.y] == '.')
				continue ;
			else if (meta->map[mp->x + c.x][mp->y + c.y] == '#')
				count++;
	}
	return (count > 0 ? match : -1);
}
