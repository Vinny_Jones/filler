/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2017/04/22 14:34:37 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*result;
	size_t	len;

	if (!s)
		return (NULL);
	while (ft_isspace(*s))
		s++;
	len = ft_strlen(s);
	while (len && ft_isspace(*(s + len - 1)))
		len--;
	result = ft_strnew(len);
	if (!result)
		return (NULL);
	result = ft_strncpy(result, s, len);
	return (result);
}
