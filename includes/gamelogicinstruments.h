/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gamelogicinstruments.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 16:24:17 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/24 16:24:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAMELOGICINSTRUMENTS_H
# define GAMELOGICINSTRUMENTS_H

int		onside(t_pair a, t_pair b, t_pair c);
int		ismap(int x, int y, t_meta *meta);

#endif
