/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cleanUp.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:42:18 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:42:18 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	cleanmetainfo(t_meta *metainfo)
{
	cleanmap(&metainfo->map);
	cleanmap(&metainfo->figure);
}

void	cleanmap(char ***map)
{
	char	**figuremap;

	figuremap = *map;
	if (!figuremap)
		return ;
	while (*figuremap)
	{
		free(*figuremap);
		figuremap++;
	}
	free(*map);
	*map = NULL;
}
