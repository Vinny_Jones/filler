/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_char.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/04 18:29:44 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/20 12:10:21 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		print_char(va_list ap, char *fmt, int num_arg, int width)
{
	unsigned char	ch;
	int				i;
	char			filler;

	filler = isnulflag(fmt) ? '0' : ' ';
	if (CFMT('l') || CFMT('C'))
		return (write_wchar(get_char(ap, fmt, num_arg), fmt, width));
	else
		ch = (unsigned char)get_char(ap, fmt, num_arg);
	i = 0;
	while (!CFMT('-') && i++ < width - 1)
		write(1, &filler, 1);
	write(1, &ch, 1);
	i = 0;
	while ((CFMT('-') || width < 0) && i++ < ABS(width) - 1)
		write(1, " ", 1);
	return (MAX(1, ABS(width)));
}

int		get_char(va_list ap, char *fmt, int num_arg)
{
	int	ch;

	if (num_arg)
		ch = (int)getnumarg(ap, ft_atoi(fmt + 1), fmt);
	else
		ch = va_arg(ap, int);
	return (ch);
}
