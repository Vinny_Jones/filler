/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   saveFigureData.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:48 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:41:49 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		savefigure(t_meta *meta)
{
	char	*inputline;
	char	*temp;
	int		error;

	inputline = NULL;
	if (!getlineadvanced(&inputline))
		return (0);
	error = (ft_strncmp(inputline, "Piece ", 6)) ? 1 : 0;
	temp = inputline + 6;
	error = (error || ft_atoi(temp) <= 0 || !ft_isdigit(*temp)) ? 1 : 0;
	meta->figureheight = ft_atoi(temp);
	temp += ft_nbrlen(meta->figureheight);
	error = (error || *temp != ' ' || ft_atoi(temp) <= 0) ? 1 : 0;
	meta->figurewidth = ft_atoi(++temp);
	temp += ft_nbrlen(meta->figurewidth);
	error = (error || ft_strcmp(temp, ":")) ? 1 : 0;
	free(inputline);
	if (error)
		return (printerror(ERR7));
	return (allocfiguremap(meta) && savefigurefield(meta));
}

int		savefigurefield(t_meta *meta)
{
	int		height;
	char	*inputline;

	height = 0;
	inputline = NULL;
	while (height < meta->figureheight && getlineadvanced(&inputline))
	{
		if (!savefigureline(height, inputline, meta))
			return (0);
		inputline = NULL;
		height++;
	}
	if (inputline)
		free(inputline);
	return ((height == meta->figureheight) ? 1 : printerror(ERR7));
}

int		allocfiguremap(t_meta *meta)
{
	int		counter;

	cleanmap(&meta->figure);
	if (!(meta->figure = MALLOC_ARRAY(char *, meta->figureheight + 1)))
		return (printerror(ERR6));
	counter = 0;
	while (counter < meta->figureheight + 1)
		(meta->figure)[counter++] = NULL;
	return (1);
}

int		savefigureline(int height, char *inputline, t_meta *meta)
{
	int		errorflag;
	char	*figureline;

	(meta->figure)[height] = inputline;
	errorflag = ((int)ft_strlen(inputline) == meta->figurewidth) ? 0 : 1;
	figureline = inputline;
	while (!errorflag && *figureline)
	{
		errorflag = (VALIDFIGURECHAR(*figureline) ? 0 : 1);
		figureline++;
	}
	return ((errorflag || *figureline) ? printerror(ERR7) : 1);
}

int		validmapchar(char c)
{
	return (c == '.' || c == 'o' || c == 'O' || c == 'x' || c == 'X');
}
