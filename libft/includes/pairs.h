/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pairs.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:40:46 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:40:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PAIRS_H
# define PAIRS_H

typedef struct	s_pair {
	int	x;
	int y;
}				t_pair;

typedef struct	s_coordinates {
	double	x;
	double	y;
}				t_coords;

t_coords		pairtocoords(t_pair *pair);
t_pair			coordstopair(t_coords *coords);
int				pairequal(t_pair *a, t_pair *b);
t_pair			makepair(int x, int y);
t_coords		makecoords(double x, double y);

#endif
