/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_round.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:12 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:26:36 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t		ft_round(double nbr)
{
	int	round;
	int sign;

	sign = (nbr < 0) ? -1 : 1;
	round = ABS((ssize_t)(nbr * 100) % 100) > 44;
	if (sign > 0)
		return ((ssize_t)nbr + round);
	return ((ssize_t)nbr - round);
}
