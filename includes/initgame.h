/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initgame.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:40:04 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:40:04 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INITGAME_H
# define INITGAME_H

int		initgame(t_meta *meta);
int		getlineadvanced(char **inputline);
int		parseuserinfo(char *inputline, t_meta *meta);
int		initiateboardsize(t_meta *meta, char *inputline);
int		allocboardmap(t_meta *meta);

#endif
