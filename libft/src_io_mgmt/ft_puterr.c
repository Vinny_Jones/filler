/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puterr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 23:39:08 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/22 23:39:08 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_puterr(char *format, char *string)
{
	ft_putstr_fd(format, 2);
	ft_putstr_fd(string, 2);
	ft_putstr_fd(FMT_OFF, 2);
}
