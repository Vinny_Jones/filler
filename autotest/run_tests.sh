#!/bin/zsh
###############################
enemies=('abanlin' 'carli' 'champely' 'grati' 'hcao' 'superjeannot')	#enemies to be tested
filler_path=$1						#path to your filler executable
resources_path=$2					#path to resources (players/ and maps/ directories should be here)
################################

autoload colors; colors

if [[ -z $filler_path || -z $resources_path ]]
then
	echo $fg[red]"Usage:"
	echo "\t ./run_tests [filler_executable] [path_to_resources]"$reset_color
	exit 0
fi

echo $fg[magenta]"Running filler tests..."$reset_color
function run_filler()
{
	enemy_path="$resources_path/players/"$enemy".filler"
	map="$resources_path/maps/map0$map_number"

	exec_command="ruby $resources_path/filler_vm -p$filler_number $filler_path -p$enemy_number $enemy_path -q -f $map"
	
	echo "Executing $exec_command"
	
	output=$(eval $exec_command | grep fin)

	p1res=$(echo $output | grep O | cut -c11-)
	p2res=$(echo $output | grep X | cut -c11-)

	echo "$output"
	if (( $p1res > $p2res && $filler_number == 1 )) || (( $p1res < $p2res && $filler_number == 2 ))
	then
    	echo $fg[green]">>>>>> Victory =)"$reset_color
	elif (( $p1res == $p2res ))
	then
		echo $fg[yellow]"====== Draw =|"$reset_color
	else
		echo $fg[red]"<<<<<< Defeat =("$reset_color
	fi
}

for enemy in $enemies; do
	echo $fg[magenta]"__________ Against $enemy __________"$reset_color
	for ((map_number = 0; map_number < 2; map_number++)); do		#map02 is turned off due to long executing
		echo $fg[magenta]"===== Testing on map0$map_number ====="$reset_color
		for filler_number in 1 2; do
			echo $fg[magenta]"--- As player number $filler_number ---"$reset_color
			for ((i = 0; i < 5; i++)); do
				if (( $filler_number == 1 )); then enemy_number=2; else enemy_number=1; fi
				run_filler
				#exit;
			done
		done
	done
done

