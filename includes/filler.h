/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:40:16 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:40:17 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# define STDIN 0

# include "libft.h"
# include "metainfo.h"
# include "initgame.h"
# include "savemapdata.h"
# include "savefiguredata.h"
# include "gamelogic.h"
# include "gamelogicinstruments.h"
# include "gamelogictools.h"
# include "drawlogic.h"
# include "enemyfielddrawer.h"
# include "drawperplogic.h"
# include "placefunctions.h"
# include "placefunctionstools.h"
# include "errors.h"
# include "cleanup.h"
# include "tools.h"

# define MAPHEADLINE "0123456789"
# define VALIDFIGURECHAR(X) ((X) == '.' || (X) == '*')

#endif
