/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pairs.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:18 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:41:18 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_pair		makepair(int x, int y)
{
	t_pair	result;

	result.x = x;
	result.y = y;
	return (result);
}

int			pairequal(t_pair *a, t_pair *b)
{
	return (a->x == b->x && a->y == b->y);
}

t_coords	makecoords(double x, double y)
{
	t_coords	result;

	result.x = x;
	result.y = y;
	return (result);
}

t_coords	pairtocoords(t_pair *pair)
{
	return (makecoords(pair->x, pair->y));
}

t_pair		coordstopair(t_coords *coords)
{
	return (makepair((int)ft_round(coords->x), (int)ft_round(coords->y)));
}
