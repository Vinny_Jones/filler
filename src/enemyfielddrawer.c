/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enemyFieldDrawer.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 15:45:21 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/24 15:45:23 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	parsefield(t_meta *meta, int (*mfunc)(t_meta *, t_pair, t_pair *))
{
	t_pair	*pp;
	t_pair	target;
	t_pair	direction;
	int		sign;

	if (meta->logic->status != FRAME)
		return ;
	pp = meta->logic->perppoints;
	sign = onside(pp[0], pp[1], coordstopair(&meta->logic->estart));
	target = makepair(0, 0);
	direction = makepair(1, 1);
	if (sign == onside(pp[0], pp[1], target))
		mfunc(meta, target, &direction);
	target = makepair(0, meta->bwidth - 1);
	direction = makepair(1, -1);
	if (sign == onside(pp[0], pp[1], target))
		mfunc(meta, target, &direction);
	target = makepair(meta->bheight - 1, 0);
	direction = makepair(-1, 1);
	if (sign == onside(pp[0], pp[1], target))
		mfunc(meta, target, &direction);
	target = makepair(meta->bheight - 1, meta->bwidth - 1);
	direction = makepair(-1, -1);
	if (sign == onside(pp[0], pp[1], target))
		mfunc(meta, target, &direction);
}

int		drawenemyframe(t_meta *meta, t_pair pt, t_pair *dir)
{
	if (!ismap(pt.x, pt.y, meta) || !isframe(meta, pt.x, pt.y) \
		|| pairequal(&pt, &meta->logic->perpexpected[0]) \
		|| pairequal(&pt, &meta->logic->perpexpected[1]))
		return (0);
	if (meta->map[pt.x][pt.y] == '.')
		meta->map[pt.x][pt.y] = '#';
	drawenemyframe(meta, makepair(pt.x + dir->x, pt.y), dir);
	drawenemyframe(meta, makepair(pt.x, pt.y + dir->y), dir);
	return (1);
}

int		drawenemyfield(t_meta *meta, t_pair pt, t_pair *dir)
{
	if (!ismap(pt.x, pt.y, meta) || !isframe(meta, pt.x, pt.y) \
		|| pairequal(&pt, &meta->logic->perppoints[0]) \
		|| pairequal(&pt, &meta->logic->perppoints[1]))
		return (0);
	if (meta->map[pt.x][pt.y] == '.')
		meta->map[pt.x][pt.y] = '#';
	populatehash(meta, pt.x + 1, pt.y);
	populatehash(meta, pt.x - 1, pt.y);
	populatehash(meta, pt.x, pt.y + 1);
	populatehash(meta, pt.x, pt.y - 1);
	drawenemyfield(meta, makepair(pt.x + dir->x, pt.y), dir);
	drawenemyfield(meta, makepair(pt.x, pt.y + dir->y), dir);
	return (1);
}

void	populatehash(t_meta *meta, int x, int y)
{
	if (!ismap(x, y, meta) || meta->map[x][y] != '.')
		return ;
	meta->map[x][y] = '#';
	populatehash(meta, x + 1, y);
	populatehash(meta, x - 1, y);
	populatehash(meta, x, y + 1);
	populatehash(meta, x, y - 1);
}
