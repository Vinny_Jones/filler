/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawperplogic.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/24 16:56:05 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/24 16:56:08 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	drawperp(t_meta *meta, t_logic *logic)
{
	t_coords	coords[4];

	createroute(meta);
	findintersections(meta, &logic->lperp, coords);
	coords[2] = pairtocoords(&meta->logic->perpcurrent[0]);
	coords[3] = pairtocoords(&meta->logic->perpcurrent[1]);
	logic->perpexpected[0] = coordstopair(&coords[0]);
	logic->perpexpected[1] = coordstopair(&coords[1]);
	if (getdist(&coords[0], &coords[2]) < getdist(&coords[0], &coords[3]))
	{
		drawhalfline(meta, coords[0], coords[2]);
		drawhalfline(meta, coords[1], coords[3]);
		return ;
	}
	drawhalfline(meta, coords[0], coords[3]);
	drawhalfline(meta, coords[1], coords[2]);
}

void	drawhalfline(t_meta *meta, t_coords a, t_coords b)
{
	t_line	line;

	if (b.x < 0)
		b = meta->logic->center;
	if (meta->logic->sidereached[getonside(meta, a)])
		return ;
	line = getlinebypoints(&a, &b);
	drawline(meta, &line, &a, &b);
}

void	findintersections(t_meta *meta, t_line *line, t_coords *res)
{
	t_pair	p;

	if (line->b)
	{
		p = makepair(0, CALCY(0, line));
		if (isframe(meta, p.x, p.y))
			res[getonside(meta, pairtocoords(&p))] = pairtocoords(&p);
		p = makepair(meta->bheight - 1, CALCY(meta->bheight - 1, line));
		if (isframe(meta, p.x, p.y))
			res[getonside(meta, pairtocoords(&p))] = pairtocoords(&p);
	}
	if (line->a)
	{
		p = makepair(CALCX(0, line), 0);
		if (isframe(meta, p.x, p.y))
			res[getonside(meta, pairtocoords(&p))] = pairtocoords(&p);
		p = makepair(CALCX(meta->bwidth - 1, line), meta->bwidth - 1);
		if (isframe(meta, p.x, p.y))
			res[getonside(meta, pairtocoords(&p))] = pairtocoords(&p);
	}
}
