/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:22:30 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:22:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_sqrt(int nbr, int offset)
{
	double	off;
	double	result;

	if (offset <= 0 || nbr <= 0)
		return (0);
	off = 1;
	while (offset--)
		off /= 10;
	result = 0;
	while (SQUARE(result + off) < nbr)
		result += off;
	return (result);
}
