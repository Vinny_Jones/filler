/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawLogic.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:28:37 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:28:37 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DRAWLOGIC_H
# define DRAWLOGIC_H

void	createroute(t_meta *meta);
void	drawline(t_meta *m, t_line *l, t_coords *p1, t_coords *p2);
t_line	getlinebypoints(t_coords *m, t_coords *n);
int		checklinecoord(t_meta *meta, t_pair *p, t_coords *p1, t_coords *p2);

#endif
