/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   placeFunctions.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:28:52 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:28:53 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLACEFUNCTIONS_H
# define PLACEFUNCTIONS_H

double	placefigure(t_meta *meta, t_pair *mp, t_coords *sp);
int		tryplace(t_meta *meta, t_pair *res, double (*foo)(t_meta *, t_pair *));
double	tryplacestart(t_meta *meta, t_pair *mp);
double	tryplaceperp(t_meta *meta, t_pair *mp);
double	tryplaceeside(t_meta *meta, t_pair *mp);

#endif
