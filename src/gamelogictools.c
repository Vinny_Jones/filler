/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gameLogicTools.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 23:29:38 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/15 23:29:38 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		checkperpplacement(t_meta *meta, t_pair *result)
{
	int		onsideindex;
	t_pair	framepoint;

	updatecurrentpoints(meta, result);
	onsideindex = getonside(meta, pairtocoords(result));
	framepoint = isonframe(meta, result);
	if (ismap(framepoint.x, framepoint.y, meta))
	{
		meta->logic->sidereached[onsideindex] = 1;
		meta->logic->perppoints[onsideindex] = framepoint;
	}
	if (meta->logic->sidereached[0] && meta->logic->sidereached[1])
		meta->logic->status = FRAME;
}

void		updatecurrentpoints(t_meta *meta, t_pair *res)
{
	t_pair	i;
	t_pair	savecurrent[2];
	int		counter;

	counter = 0;
	savecurrent[0] = meta->logic->perpcurrent[0];
	savecurrent[1] = meta->logic->perpcurrent[1];
	i.x = -1;
	while (++i.x < meta->figureheight)
	{
		i.y = -1;
		while (++i.y < meta->figurewidth)
		{
			if (meta->figure[i.x][i.y] == '.')
				continue;
			updatecurrent(meta, makepair(res->x + i.x, res->y + i.y));
			if (meta->map[res->x + i.x][res->y + i.y] == '#')
				counter++;
		}
	}
	if (counter)
		return ;
	meta->logic->perpcurrent[0] = savecurrent[0];
	meta->logic->perpcurrent[1] = savecurrent[1];
}

void		updatecurrent(t_meta *meta, t_pair mp)
{
	t_logic	*lgc;
	int		sidx;
	double	dcurr;
	double	dnew;

	lgc = meta->logic;
	sidx = getonside(meta, pairtocoords(&mp));
	dcurr = getdistpair(&lgc->perpexpected[sidx], &lgc->perpcurrent[sidx]);
	dnew = getdistpair(&lgc->perpexpected[sidx], &mp);
	if (dnew < dcurr || lgc->perpcurrent[sidx].x < 0)
	{
		lgc->perpcurrent[sidx] = mp;
	}
}

t_pair		isonframe(t_meta *meta, t_pair *mp)
{
	t_pair	c;
	t_pair	result;

	result = makepair(-1, -1);
	c.x = -1;
	while (++c.x < meta->figureheight)
	{
		c.y = -1;
		while (++c.y < meta->figurewidth)
			if (meta->figure[c.x][c.y] == '*'
				&& isframe(meta, mp->x + c.x, mp->y + c.y))
				return (makepair(mp->x + c.x, mp->y + c.y));
	}
	return (result);
}

t_coords	calculatefigurecenter(t_meta *meta, int (*isfill)(t_meta *, int))
{
	t_pair		c;
	t_coords	result;
	double		counter;

	counter = 0;
	result = makecoords(0, 0);
	c.x = -1;
	while (++c.x < meta->bheight)
	{
		c.y = -1;
		while (++c.y < meta->bwidth)
		{
			if (isfill(meta, meta->map[c.x][c.y]))
			{
				result.x += c.x;
				result.y += c.y;
				counter++;
			}
		}
	}
	result.x /= counter;
	result.y /= counter;
	return (result);
}
